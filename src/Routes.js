import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { PATH } from './constants/path';
import { Home } from './page/Home';

export const Routes = () => {
	return (
		<Switch>
			<Route path={PATH.home} exact={true} component={Home} />
		</Switch>
	);
};
