import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import './Datepicker.scss';

import 'react-datepicker/dist/react-datepicker.css';

export const DatePickerBamos = (props) => {
	const [ date, setDate ] = useState(moment(new Date()));
	const [ value, setValue ] = useState('Date');
	const handleChange = (date) => {
		setDate(date);
		setValue('');
	};
	return (
		<div className="datepicker">
			<DatePicker
				selected={date}
				value={value ? value : date}
				onChange={handleChange}
				className="datepicker-content"
			/>
			<img src={props.icon} alt="datepicker" className="datepicker-img" />
		</div>
	);
};
