import React from 'react';
import { Link } from 'react-router-dom';
import './Dropdown.scss';

export const Dropdown = (props) => {
	const { dataItems, value, icon } = props;
	return (
		<div className="dropdown">
			<button
				className="btn btn-secondary dropdown-toggle dropdown-button"
				type="button"
				id="dropdownMenuButton"
				data-toggle="dropdown"
				aria-haspopup="true"
				aria-expanded="false"
			>
				{value}
				<img src={icon} alt="icon" className="dropdown-button-icon" />
			</button>

			<div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
				{dataItems ? (
					dataItems.map((el, index) => (
						<Link className="dropdown-item" key={index} to={el.link}>
							{el.value}
						</Link>
					))
				) : null}
			</div>
		</div>
	);
};

export const DropdownNoBoderRadius = (props) => {
	const { dataItems, value, icon } = props;
	return (
		<div className="dropdown">
			<button
				className="btn btn-secondary dropdown-toggle dropdown-button-1"
				type="button"
				id="dropdownMenuButton"
				data-toggle="dropdown"
				aria-haspopup="true"
				aria-expanded="false"
				style={{ width: props.width ? props.width : 225 }}
			>
				{value}
				<img src={icon} alt="icon" className="dropdown-button-icon" />
			</button>
			<div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
				{dataItems ? (
					dataItems.map((el, index) => (
						<Link className="dropdown-item" key={index} to={el.link}>
							{el.value}
						</Link>
					))
				) : null}
			</div>
		</div>
	);
};
