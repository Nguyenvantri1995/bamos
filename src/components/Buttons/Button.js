import React from 'react';
import './Button.scss';

export const Button = (props) => {
	const { background, border, marginRight, color, width } = props;
	return (
		<button
			type="button"
			className="btn btn-primary button"
			style={{
				width: width ? width : 164,
				height: 40,
				background: background,
				border: border,
				marginRight: marginRight ? marginRight : 0,
				color: color
			}}
		>
			{props.value}
		</button>
	);
};

export const ButtonLocalisation = (props) => {
	const { icon, value } = props;
	return (
		<button className="btn btn-secondary localisation-button" type="button">
			{value}
			<img src={icon} alt="icon" className="dropdown-button-icon" />
		</button>
	);
};
