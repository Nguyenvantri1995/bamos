import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import './Header.scss';
import logo from 'src/assets/images/logo.svg';
import { Button, ButtonLocalisation } from 'src/components/Buttons/Button';
import { Dropdown, DropdownNoBoderRadius } from 'src/components/Dropdown/Dropdown';
import iconArrowDown from 'src/assets/images/arrow-down.svg';
import iconLocation from 'src/assets/images/location.svg';

import 'react-datepicker/dist/react-datepicker.css';
import { DatePickerBamos } from 'src/components/Datepicker/DatePicker';

export const Header = () => {
	const [ showIcon, setShowIcon ] = useState(true);
	const [ showMenu, setShowmenu ] = useState(true);
	const [ active, setActive ] = useState({
		0: false,
		1: false,
		2: false,
		3: true
	});
	useEffect(
		() => {
			if (window.screen.availWidth < 991) {
				setShowIcon(false);
			} else {
				setShowIcon(true);
			}
		},
		[ showIcon ]
	);

	const handleClick = (key) => () => {
		const defaultActive = {
			0: false,
			1: false,
			2: false,
			3: false
		};
		defaultActive[key] = !active[key];
		setActive(defaultActive);
	};

	const dataMenu = [
		{
			link: '#',
			lable: 'Inscription'
		},
		{
			link: '#',
			lable: 'Connexion'
		},
		{
			link: '#',
			lable: 'S’BNetwork'
		},
		{
			link: '#',
			lable: 'Créer un event'
		}
	];

	const dataUniversPrincipale = [
		{
			value: 'Particulier',
			link: '#'
		},
		{
			value: 'Pro',
			link: '#'
		},
		{
			value: 'S’BNetwork',
			link: '#'
		}
	];

	const dataUniversSecondaire = [
		{
			value: 'Ateliers',
			link: '#'
		},
		{
			value: 'Beauté',
			link: '#'
		},
		{
			value: 'Culturel',
			link: '#'
		},
		{
			value: 'Épicurien',
			link: '#'
		},
		{
			value: 'Event S’Bamos',
			link: '#'
		},
		{
			value: 'Famille',
			link: '#'
		},
		{
			value: 'Formations',
			link: '#'
		},
		{
			value: 'Insolite',
			link: '#'
		},
		{
			value: 'Jeux',
			link: '#'
		},
		{
			value: 'Santé & Bien-être',
			link: '#'
		},
		{
			value: 'Fêtes',
			link: '#'
		},
		{
			value: 'Sports',
			link: '#'
		}
	];

	return (
		<div className="header">
			<div className="menu">
				<nav className="navbar navbar-expand-lg navbar-light bg-light">
					<img src={logo} alt="logo" />
					<button
						className="navbar-toggler"
						type="button"
						data-toggle="collapse"
						data-target="#navbarSupportedContent"
						aria-controls="navbarSupportedContent"
						aria-expanded="false"
						aria-label="Toggle navigation"
					>
						<span className="navbar-toggler-icon" />
					</button>

					<div className="collapse navbar-collapse header-menu" id="navbarSupportedContent">
						{showMenu ? (
							<ul className="navbar-nav">
								{dataMenu.map((el, index) => (
									<li className={`nav-item ${active[index] ? 'active' : null}`} key={index}>
										<Link
											className={`nav-link header-menu-item ${active[index] ? 'active' : null}`}
											to={el.link}
											onClick={handleClick(index)}
										>
											{el.lable}
										</Link>
									</li>
								))}
							</ul>
						) : null}

						{showIcon ? (
							<div className="form-inline my-2 my-lg-0 icon-menu">
								<i className="fas fa-bars fa-3x " onClick={() => setShowmenu(!showMenu)} />
							</div>
						) : null}
					</div>
				</nav>
			</div>
			<div className="content">
				<span>Une soirée, un atelier, une formation… C’est vous qui choississez !</span>
			</div>
			<div className="button">
				<Button value="Organisez" background="transparent" border="1px solid white" marginRight={32} />
				<Button value="Participez" background="transparent" border="1px solid white" />
			</div>
			<div className="header-dropdown">
				<Dropdown dataItems={dataUniversPrincipale} value="Univers principale" icon={iconArrowDown} />
				<DropdownNoBoderRadius
					dataItems={dataUniversSecondaire}
					value="Univers secondaire"
					icon={iconArrowDown}
				/>
				<ButtonLocalisation value="Localisation" icon={iconLocation} />
				<DatePickerBamos icon={iconArrowDown} />

				<DropdownNoBoderRadius
					dataItems={dataUniversPrincipale}
					value="Prix"
					icon={iconArrowDown}
					width={155}
				/>
				<DropdownNoBoderRadius dataItems={dataUniversPrincipale} value="Age" icon={iconArrowDown} width={177} />
			</div>
		</div>
	);
};
