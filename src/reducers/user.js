const initalUser = {
	success: false,
	data: [],
	meta: {
		next: '',
		previous: null,
		count: 0,
		limit: 0,
		offset: 0
	}
};

export const users = (state = initalUser, action) => {
	switch (action.type) {
		case SET_USER:
			return action.payload;

		default:
			return state;
	}
};
