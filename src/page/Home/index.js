import React from 'react';

import { Header } from 'src/layout/Header';
import { ContentHome } from './content';

export const Home = () => {
	return (
		<div className="home">
			<Header />
			<ContentHome />
		</div>
	);
};
