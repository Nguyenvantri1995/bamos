import React from 'react';
import './content.scss';
import imageCardOne from 'src/assets/images/card-one.png';
import imageCardTwo from 'src/assets/images/card-two.svg';
import imageCardThree from 'src/assets/images/63d606bf509e79f21a527aaa5def9744f1503f70.png';
import imageCardFour from 'src/assets/images/d1a763f7815d62db7aff13b8d3b8ba5a5dedc8e3.png';
import imageCardFive from 'src/assets/images/f03c279372d6e6030faf734f846dbdad7e945c20.png';
import imageCardSix from 'src/assets/images/9ffe61a8645abc79db496ded0aa63287512e5fb8.png';
import imageCardSeven from 'src/assets/images/3a1b23629fafba3236ec88b2643d234b610a6145.png';

import imageAvatarCard from 'src/assets/images/avatar-card-one.png';
import imageAvatarCardTow from 'src/assets/images/avatar-card-two.png';
import imageAvatarCardThree from 'src/assets/images/b1915c29550060d981e014c10e7efe7333eba5a3.png';
import imageAvatarCardFour from 'src/assets/images/80b43ae0ed021c39004ca7710c4cb80262009226.png';
import imageAvatarCardFive from 'src/assets/images/30276aaf999e9e2c4fc395d358200393b68543da.png';
import { CardEventMinute } from './component/CardEventMinute';
import { CardEventMinuteOne } from './component/CardEventMinuteOne';
import { Button } from 'src/components/Buttons/Button';
import { CardEventMinuteTwo } from './component/CardEventMinuteTwo';
import { CardEventMinuteThree } from './component/CardEventMinuteThree';
import { CardEventMinuteFour } from './component/CardEventMinuteFour';

export const ContentHome = () => {
	const dataCardEventMinute = [
		{
			imageCard: imageCardOne,
			avatar: imageAvatarCard,
			titleLeft: 'épicurien',
			titleRight: 'PRO',
			description: 'Why Do Make Ahead Recipes Work So Well To Reduce Your Dinner Party',
			name: 'Christine Caldwell',
			calendar: '30 nov - 15h00',
			mapMarke: 'Tatumburgh',
			money: '20€'
		},
		{
			imageCard: imageCardTwo,
			avatar: imageAvatarCardTow,
			titleLeft: 'Beauté',
			titleRight: '',
			description: 'Fun Grill Sizzle',
			name: 'Joseph Bowen',
			calendar: '28 nov - 13h00',
			mapMarke: 'Gomzikze',
			money: '93€'
		}
	];

	const dataCardEventMinuteOne = [
		{
			imageCard: imageCardThree,
			avatar: imageAvatarCardThree,
			titleLeft: 'Famille',
			titleRight: '',
			description: 'Trip To Iqaluit In Nunavut A Canadian Arctic City',
			name: 'Christine Caldwell',
			calendar: '30 nov - 15h00',
			mapMarke: 'Brianberg',
			money: '55€'
		},
		{
			imageCard: imageCardFour,
			avatar: imageAvatarCardTow,
			titleLeft: 'Beauté',
			titleRight: '',
			description: 'Fun Grill Sizzle',
			name: 'Joseph Bowen',
			calendar: '28 nov - 13h00',
			mapMarke: 'Gomzikze',
			money: '93€',
			showImage: true
		},
		{
			imageCard: imageCardFive,
			avatar: imageAvatarCardFour,
			titleLeft: 'Ateliers',
			titleRight: '',
			description: 'Mother Earth Hosts Our Travels',
			name: 'Joseph Bowen',
			calendar: '28 nov - 13h00',
			mapMarke: 'Piblucoh',
			money: '59€'
		}
	];

	const dataCardEventMinuteTow = [
		{
			imageCard: imageCardSix,
			avatar: imageAvatarCardFive,
			titleLeft: 'Culture',
			titleRight: '',
			description: 'Alfredo And Who On Earth Was He',
			name: 'Bernard Willis',
			calendar: '29 nov - 14h45',
			mapMarke: 'Ulasuhwoc',
			money: '85€'
		},
		{
			imageCard: imageCardSeven,
			avatar: imageAvatarCardFive,
			titleLeft: 'Fêtes',
			titleRight: '',
			description: 'Messes Make Memories',
			name: 'Phillip Holland',
			calendar: '27 nov - 10h30',
			mapMarke: 'Vegligo',
			money: '30€'
		}
	];

	return (
		<div className="home-content">
			<div className="home-content-wapper">
				<div className="item-1">
					<div className="card-item">
						<div className="card-item-1">
							<span className="title">Event minute</span>
						</div>
						<div className="row">
							{dataCardEventMinute.map((el, index) => (
								<div className="col-md-6 col-sm-12 col-xs-12" key={index}>
									<CardEventMinute data={el} />
								</div>
							))}
						</div>
					</div>
					<div className="card-item-2">
						<div className="row">
							{dataCardEventMinuteOne.map((el, index) => (
								<div className="col-md-4 col-sm-12 col-xs-12" key={index}>
									<CardEventMinuteOne data={el} />
								</div>
							))}
						</div>
					</div>

					<div className="card-item">
						<div className="row">
							{dataCardEventMinuteTow.map((el, index) => (
								<div className="col-md-6 col-sm-12 col-xs-12" key={index}>
									<CardEventMinute data={el} />
								</div>
							))}
						</div>
					</div>

					<div className="button-organisez">
						<Button
							value="Organisez"
							background="transparent"
							border="1px solid #5a555a"
							marginRight={32}
							color="#5a555a"
						/>
					</div>
				</div>
				<div className="item-2">
					<div className="card-item">
						<div className="row">
							<CardEventMinuteTwo />
						</div>
					</div>
				</div>
				<div className="item-3">
					<div className="card-item">
						<div className="row">
							<CardEventMinuteThree />
						</div>
					</div>
				</div>
				<div className="item-4">
					<div className="card-item">
						<div className="row">
							<CardEventMinuteFour />
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};
