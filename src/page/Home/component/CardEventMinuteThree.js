import React from 'react';
import './CardEventMinuteThree.scss';
import { Button } from 'src/components/Buttons/Button';
import imageCard from 'src/assets/images/544d3de83b9655a8e9bd46c45d9a16e554820d2f.png';

export const CardEventMinuteThree = () => {
	return (
		<div className="card-3">
			<div className="col-md-5 col-xs-12 card-right">
				<img src={imageCard} alt="cardImage" className="card-right-image" />
			</div>
			<div className="col-md-7 col-xs-12 card-left">
				<div className="content">
					<span className="title">Osez organiser</span>
					<p className="description">
						Profitez de notre économie collaborative créé dans le seul but de vous récompensez. Proposer vos
						meilleurs offres pour que le prix ne soit plus un frein à la participation. Particuliers ou
						professionnels testez vos idées gratuitement et faites appel à la tribu S’Bamos ! <br /> La
						seule limite ? Votre imagination
					</p>

					<div className="card-3-button">
						<Button
							value="Créez votre event"
							background="linear-gradient(180deg, #2EBAEB 0%, #007DA9 100%)"
							border="1px solid white"
							marginRight={29}
						/>
						<Button
							value="En savoir plus"
							background="transparent"
							border="1px solid #00abe7"
							color="#00abe7"
							width={133}
						/>
					</div>
				</div>
			</div>
		</div>
	);
};
