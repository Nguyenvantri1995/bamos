import React from 'react';
import './CardEventMinuteTwo.scss';
import { Button } from 'src/components/Buttons/Button';
import imageCard from 'src/assets/images/984bb6b7cc02816363dcf72942d63719511d192e.png';

export const CardEventMinuteTwo = () => {
	return (
		<div className="card-2">
			<div className="col-md-7 col-xs-12 card-left">
				<div className="content">
					<span className="title">Vous allez adorer participer</span>
					<p className="description">
						Plongez dans l’univers S’Bamos à la recherche d’un event gratuit, payant ou privé. Un instant de
						détente, une folle envie de faire la fête ou de vivre un moment de partage, invitez vos proches
						à l’aide de votre code parrain. Agrandissez votre tribu et l’économie collaborative made in
						S’Bamos remplira votre tirelire.
					</p>

					<div className="card-2-button">
						<Button
							value="Trouvez un event"
							background="linear-gradient(180deg, #FA7FA8 0%, #F02A64 100%)"
							border="1px solid white"
							marginRight={29}
						/>
						<Button
							value="En savoir plus"
							background="transparent"
							border="1px solid #f94f88"
							color="#f94f88"
							width={133}
						/>
					</div>
				</div>
			</div>

			<div className="col-md-5 col-xs-12 card-right">
				<img src={imageCard} alt="cardImage" className="card-right-image" />
			</div>
		</div>
	);
};
