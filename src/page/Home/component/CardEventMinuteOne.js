import React from 'react';
import iconCalendar from 'src/assets/images/calendrier.svg';
import iconMapMarke from 'src/assets/images/map-marke.svg';
import iconMoney from 'src/assets/images/money.svg';
import './CardEventMinuteOne.scss';

export const CardEventMinuteOne = (props) => {
	const { imageCard, avatar, titleLeft, titleRight, description, calendar, mapMarke, money, showImage } = props.data;
	console.log(showImage);
	return (
		<div className="card-content">
			{showImage ? (
				<img src={imageCard} alt="imageCard" className="card-content-img" />
			) : (
				<React.Fragment>
					<img src={imageCard} alt="imageCard" className="card-content-img" />
					<img src={avatar} alt="avatar-card" className="avatar" />
					<div className="card-content-bottom">
						<div className="content">
							<div className="content-left">
								<div className="title">
									<span className="title-left">{titleLeft}</span>
									{titleRight ? <span className="title-right">{titleRight}</span> : null}
								</div>
								<p className="content-left-description">{description}</p>
								<div className="footer">
									<div className="footer-item-1">
										<img src={iconCalendar} alt="calendar" />
										<span className="test">{calendar}</span>
									</div>
									<div className="footer-item-1">
										<img src={iconMapMarke} alt="calendar" />
										<span className="test">{mapMarke}</span>
									</div>
									<div className="footer-item-1">
										<img src={iconMoney} alt="calendar" />
										<span className="test">{money}</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</React.Fragment>
			)}
		</div>
	);
};
