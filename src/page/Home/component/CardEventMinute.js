import React from 'react';
import iconCalendar from 'src/assets/images/calendrier.svg';
import iconMapMarke from 'src/assets/images/map-marke.svg';
import iconMoney from 'src/assets/images/money.svg';
import './CardEventMinute.scss';

export const CardEventMinute = (props) => {
	const { imageCard, avatar, titleLeft, titleRight, description, name, calendar, mapMarke, money } = props.data;
	return (
		<div className="card-content">
			<img src={imageCard} alt="imageCard" className="card-content-img" />
			<div className="card-content-bottom">
				<div className="content">
					<div className="content-left">
						<div className="title">
							<span className="title-left">{titleLeft}</span>
							{titleRight ? <span className="title-right">{titleRight}</span> : null}
						</div>
						<p className="content-left-description">{description}</p>
						<div className="footer">
							<div className="footer-item-1">
								<img src={iconCalendar} alt="calendar" />
								<span className="test">{calendar}</span>
							</div>
							<div className="footer-item-1">
								<img src={iconMapMarke} alt="calendar" />
								<span className="test">{mapMarke}</span>
							</div>
							<div className="footer-item-1">
								<img src={iconMoney} alt="calendar" />
								<span className="test">{money}</span>
							</div>
						</div>
					</div>

					<div className="content-right">
						<img src={avatar} alt="avatar-card" className="avatar" />
						<p className="test">{name}</p>
						<div className="footer-item">
							<i className="fas fa-star star-1 fa-2x" />
							<i className="fas fa-star star-2 fa-2x" />
							<i className="fas fa-star star-3 fa-2x" />
							<i className="fas fa-star star-4 fa-2x" />
							<i className="fas fa-star star-5 fa-2x" />
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};
