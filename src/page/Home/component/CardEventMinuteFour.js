import React from 'react';
import './CardEventMinuteFour.scss';
import { Button } from 'src/components/Buttons/Button';
import imageCard from 'src/assets/images/d423f610193312a3d1e39da12f3b80571c47b5b0.png';

export const CardEventMinuteFour = () => {
	return (
		<div className="card-4">
			<div className="col-md-7 col-xs-12 card-left">
				<div className="content">
					<span className="title">Agrandissez votre tribu</span>
					<p className="description">
						Votre tirelire parrainage atteint 50€ ? Encore un coup de l’économie collaborative ! Pas de
						limite sur S’Bamos, invitez autant de proches que vous le désirez en partageant votre code
						parrain. N’oubliez pas les professionnels, ils rempliront votre tirelire en un clin d’œil ! Et
						hop 180€ reversé par S’Bamos pour avoir parrainé un pro à l’abonnement S’BNetwork. Que vos
						filleuls participent, organisent ou s’abonnent, votre tirelire se remplie tous les mois et à
						vie !
					</p>

					<div className="card-4-button">
						<Button
							value="Invitez"
							background="linear-gradient(180deg, #5A555A 0%, #24282A 100%)"
							border="1px solid white"
							width={133}
							marginRight={29}
						/>
						<Button
							value="En savoir plus"
							background="transparent"
							border="1px solid #5a555a"
							color="#5a555a"
						/>
					</div>
				</div>
			</div>

			<div className="col-md-5 col-xs-12 card-right">
				<img src={imageCard} alt="cardImage" className="card-right-image" />
			</div>
		</div>
	);
};
