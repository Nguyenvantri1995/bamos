import React from 'react';
import { Router } from 'react-router-dom';
import { Routes } from './Routes';

const App = ({ history }) => {
	return (
		<Router history={history}>
			<Routes />
		</Router>
	);
};

export default App;
